# Floodlight

***An Intership Management Platform***

## Introduction

This web platform was developed for the RedLight SI Dev Challenge.

The main objective of the application is to help manage and keep track of diffrent internship roles within a company.

## Tech Stack

##### Frontend

* HTML
* CSS - Bootstrap and Google Fonts

##### Backend

* Django
* SQLite ( The project could easily be converted to work with a Postgres database but would envolve migrating and having a Postgres server running aside, for example in a Docker container, which would bring unnecessary complexity to a project this size in my opinion.)

## Setting up the project

##### Requirements

* Python 3.7.9 or later

##### Set up

1. Navigate to /**src** directory.
2. Activate the Python virtual environment with the following command:

   ```
   .\env\Scripts\activate
   ```
3. Navigate to /webapp directory.
4. Run the following command:

   ```
   python manage.py runserver
   ```

Once you have completed all steps the application should be running. You can access it in your web browser at :  **[http://localhost:8000/](http://localhost:8000/).**

## Development

##### The Idea

The development started with the analysis of the requirements and choice of technology. I chose a technology I had used before and so I was confortable with.

Then I looked over the models I needed to create, mainly Applicants and Roles and thought about relationships and connections between them that would allow me to achieve what was asked. I ended up creating an "intermediate" model called ApplicantRole that is related to both an Applicant and a Role and keeps a status for the application.

Then came the time to create the Django project and import the necessary libraries.

##### Starting to code

I started by creating the most crucial endpoints, mainly the ones that involve creating applicants and roles and listing them.

Then coming from there I wrote code for the features that rely on those, being the ones to search, update and delete objects.

Lastly I added the logic to switch status on applications and a filter on the applicants page that allows to only search for applicants which have at least an application to a role with a status of the selected type.

As I developed the endpoints and the logic behind them, I also wrote the code for the frontend part so I could get a better view of my work and improve what was still missing, as this was done alone.

##### Final touches

After having the main idea finished, I polished up aspects I thought were less appealing, mainly in the frontend.

Then it came down to testing the application and trying to find any bugs I might have missed.
