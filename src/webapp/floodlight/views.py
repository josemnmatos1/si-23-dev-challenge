from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from floodlight.forms import create_applicant_form, create_role_form
from floodlight.models import Applicant, Role, ApplicantRole


# Create your views here.

# GENERAL -----------------------------------------------------------------------------------------------


def index(request):
    return render(request, "floodlight/index.html")


def home(request):
    # get all applicants
    applicants = Applicant.objects.all()

    # get all roles
    roles = Role.objects.all()

    # get all approved applicants profile and role
    approved_applicants_details = ApplicantRole.objects.filter(status="approved").values_list('applicant__name', 'role__name')


    # get all rejected applicants profile and role
    rejected_applicants_details = ApplicantRole.objects.filter(status="rejected").values_list('applicant__name', 'role__name')


    # get all under analysis applicants profile and role
    under_analysis_applicants_details = ApplicantRole.objects.filter(status="under_analysis").values_list('applicant__name', 'role__name')

    # get the count of each
    role_count = len(roles)
    applicant_count = len(applicants)
    approved_applicant_count = len(approved_applicants_details)
    rejected_applicant_count = len(rejected_applicants_details)
    under_analysis_applicant_count = len(under_analysis_applicants_details)



    status = {
        "role_count": role_count,
        "applicant_count": applicant_count,
        "approved_applicant_count": approved_applicant_count,
        "rejected_applicant_count": rejected_applicant_count,
        "under_analysis_applicant_count": under_analysis_applicant_count,
        "approved_applicants": approved_applicants_details,
        "rejected_applicants": rejected_applicants_details,
        "under_analysis_applicants": under_analysis_applicants_details,
    }

    return render(request, "floodlight/home.html", {"status": status})


# APPLICANTS ---------------------------------------------------------------------------------------------


def applicants(request):
    query = request.GET.get("query")
    status = request.GET.get("status")
    applicants = Applicant.objects.all()

    if query:
        # Filter applicants by name or email
        applicants = applicants.filter(name__icontains=query) | applicants.filter(
            email__icontains=query
        )

    if status == "approved":
        # select all approved applicants for at least one role
        applicants = applicants.filter(applicantrole__status="approved").distinct()

    elif status == "under_analysis":
        # select all under analysis applicants for at least one role
        applicants = applicants.filter(
            applicantrole__status="under_analysis"
        ).distinct()

    elif status == "rejected":
        # select all rejected applicants for at least one role
        applicants = applicants.filter(applicantrole__status="rejected").distinct()

    return render(
        request,
        "floodlight/applicants.html",
        {"applicants": applicants, "query": query, "status": status},
    )


def create_applicant(request):
    if request.method == "POST":
        # requested to the user
        form = create_applicant_form(request.POST, request.FILES)
        try:
            if form.is_valid():
                applicant = form.save()
                selected_roles = request.POST.getlist(
                    "roles"
                )  # Get the selected roles from the form
                for role_id in selected_roles:
                    role = Role.objects.get(id=role_id)
                    ApplicantRole.objects.create(
                        applicant=applicant, role=role, status="under_analysis"
                    )

                return redirect("applicant_page", applicant_id=applicant.id)

        except Exception as e:
            print(e)
            return HttpResponse("Error creating applicant")

    else:
        form = create_applicant_form()

    return render(request, "floodlight/create_applicant.html", {"form": form})


def update_applicant(request, applicant_id):
    try:
        applicant = Applicant.objects.get(id=applicant_id)
    except Applicant.DoesNotExist:
        raise Http404("Applicant does not exist")

    if request.method == "POST":
        form = create_applicant_form(request.POST, request.FILES, instance=applicant)
        if form.is_valid():
            form.save()

            # Update ApplicantRole instances
            selected_roles = request.POST.getlist("roles")

            current_roles = ApplicantRole.objects.filter(
                applicant=applicant
            ).values_list("role__id", flat=True)

            # Remove applicant from roles not selected anymore
            roles_to_remove = set(current_roles) - set(selected_roles)
            for role_id in roles_to_remove:
                role = get_object_or_404(Role, id=role_id)
                ApplicantRole.objects.filter(applicant=applicant, role=role).delete()

            # Add applicant to new selected roles
            roles_to_add = set(selected_roles) - set(current_roles)
            for role_id in roles_to_add:
                role = get_object_or_404(Role, id=role_id)
                ApplicantRole.objects.create(
                    applicant=applicant, role=role, status="under_analysis"
                )

            return redirect("applicant_page", applicant_id=applicant_id)

    else:
        form = create_applicant_form(instance=applicant)

    return render(request, "floodlight/update_applicant.html", {"form": form})


def delete_applicant(request, applicant_id):
    try:
        applicant = Applicant.objects.get(id=applicant_id)
        applicant.delete()
        return redirect("applicants")
    except Applicant.DoesNotExist:
        raise Http404("Applicant does not exist")


def applicant_page(request, applicant_id):
    try:
        applicant = Applicant.objects.get(id=applicant_id)

        applications = []

        # get applicant roles
        for role in ApplicantRole.objects.filter(applicant=applicant):
            applications.append(role)

    except Applicant.DoesNotExist:
        raise Http404("Applicant does not exist")
    return render(
        request,
        "floodlight/applicant_page.html",
        {"applicant": applicant, "applications": applications},
    )


# ROLES -------------------------------------------------------------------------------------------------


def roles(request):
    query = request.GET.get("query")
    roles = Role.objects.all()

    if query:
        roles = roles.filter(name__icontains=query)

    return render(
        request,
        "floodlight/roles.html",
        {"roles": roles, "query": query},
    )


def create_role(request):
    if request.method == "POST":
        # requested to the user
        form = create_role_form(request.POST, request.FILES)
        if form.is_valid():
            name = form.cleaned_data["name"]
            role = Role(name=name)
            role.save()
            return redirect("roles")

    else:
        form = create_role_form()

    return render(request, "floodlight/create_role.html", {"form": form})


def update_role(request, role_id):
    # use update role form
    try:
        role = Role.objects.get(id=role_id)
    except Role.DoesNotExist:
        raise Http404("Role does not exist")

    if request.method == "POST":
        form = create_role_form(request.POST, request.FILES, instance=role)
        if form.is_valid():
            form.save()
            return redirect("role_page", role_id=role_id)

    else:
        form = create_role_form(instance=role)

    return render(request, "floodlight/update_role.html", {"form": form})


def role_page(request, role_id):
    try:
        role = Role.objects.get(id=role_id)
    except Role.DoesNotExist:
        raise Http404("Role does not exist")

    applicants_with_role = []
    for applicant in ApplicantRole.objects.filter(role=role):
        applicants_with_role.append(applicant)

    return render(
        request,
        "floodlight/role_page.html",
        {"role": role, "applicants": applicants_with_role},
    )


def delete_role(request, role_id):
    try:
        role = Role.objects.get(id=role_id)
        role.delete()
        return redirect("roles")
    except Role.DoesNotExist:
        raise Http404("Role does not exist")


# APPLICANT ROLE ----------------------------------------------------------------------------------------


def update_status(request, applicant_role_id, new_status):
    try:
        if new_status not in ["under_analysis", "approved", "rejected"]:
            raise Exception("Invalid status")
        applicant_role = ApplicantRole.objects.get(id=applicant_role_id)
        applicant_role.status = new_status
        applicant_role.save()
        return redirect("role_page", role_id=applicant_role.role.id)

    except ApplicantRole.DoesNotExist:
        raise Http404("Error updating status.")
