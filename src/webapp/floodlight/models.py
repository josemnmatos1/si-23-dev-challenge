from django.db import models

class Role(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Applicant(models.Model):
    name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15)
    email = models.EmailField()
    avatar = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.name


class ApplicantRole(models.Model):
    
    STATUS_CHOICES = [
        ('approved', 'Approved'),
        ('rejected', 'Rejected'),
        ('under_analysis', 'Under Analysis'),
    ]
        
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)


    def __str__(self):
        return f"{self.applicant_id} - {self.role_id} - {self.status}"