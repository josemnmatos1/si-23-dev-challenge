from django import forms
from .models import Applicant, Role, ApplicantRole


class create_applicant_role_form(forms.ModelForm):
    class Meta:
        model = ApplicantRole
        fields = ["status"]


class create_applicant_form(forms.ModelForm):
    roles = forms.ModelMultipleChoiceField(
        queryset=Role.objects.all(), widget=forms.CheckboxSelectMultiple
    )

    applicant_roles = forms.inlineformset_factory(
        Applicant,
        ApplicantRole,
        form=create_applicant_role_form,
        extra=0,
        can_delete=False,
    )

    class Meta:
        model = Applicant
        fields = ["name", "phone_number", "email", "avatar"]


class create_role_form(forms.ModelForm):
    class Meta:
        model = Role
        fields = ["name"]



