from django.apps import AppConfig


class FloodlightConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'floodlight'
