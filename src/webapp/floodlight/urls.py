from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("home", views.home, name="home"),
    path("applicants", views.applicants, name="applicants"),
    path("create_applicant", views.create_applicant, name="create_applicant"),
    path("applicant_page/<int:applicant_id>", views.applicant_page, name="applicant_page"),
    path("roles", views.roles, name="roles"),
    path("create_role", views.create_role, name="create_role"),
    path("update_role/<int:role_id>", views.update_role, name="update_role"),
    path("role_page/<int:role_id>", views.role_page, name="role_page"),
    path("update_applicant/<int:applicant_id>", views.update_applicant, name="update_applicant"),
    path("delete_applicant/<int:applicant_id>", views.delete_applicant, name="delete_applicant"),
    path("delete_role/<int:role_id>", views.delete_role, name="delete_role"),
    path("update_status/<int:applicant_role_id>/<str:new_status>", views.update_status, name="update_status")
]