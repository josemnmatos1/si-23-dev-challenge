# Generated by Django 3.2.19 on 2023-05-24 19:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('floodlight', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicant',
            name='avatar',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
